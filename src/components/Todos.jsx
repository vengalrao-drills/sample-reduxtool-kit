import React from 'react'
import {  useDispatch, useSelector } from 'react-redux'
import { removeTodo } from '../features/todo/todoSlice'

const Todos = () => {
    const todos = useSelector(state=>state.todos)
    console.log(todos) 
    const dispatch = useDispatch();

    const clicked = (id) =>{
        dispatch(removeTodo(id))
    }

  return (
    <div>
      <div>Todos</div>
      {todos.map((todo)=>( 
        <div key={todo.id} > {todo.todo} <button onClick={()=>clicked(todo.id)} >delete</button>  </div>
      ))} 
    </div>
  )
}

export default Todos
